package Ingress.az.eTaskify.controller;
import Ingress.az.eTaskify.dto.AuthRequestDto;
import Ingress.az.eTaskify.dto.JwtResponseDto;
import Ingress.az.eTaskify.dto.RefreshTokenRequestDto;
import Ingress.az.eTaskify.service.AuthService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class LoginController {
    private final AuthService authenticationService;

    @PostMapping("/login")
    public JwtResponseDto login(@RequestBody AuthRequestDto authRequestDto) {
        return authenticationService.login(authRequestDto);
    }



    @PostMapping("/refreshToken")
    public JwtResponseDto refreshToken(@RequestBody RefreshTokenRequestDto refreshTokenRequestDto) {
        return authenticationService.refreshToken(refreshTokenRequestDto);
    }
}
