package Ingress.az.eTaskify.controller;
import Ingress.az.eTaskify.dto.TaskDto;
import Ingress.az.eTaskify.dto.TaskResponseDto;
import Ingress.az.eTaskify.service.TaskService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class TaskController {
    private final TaskService taskService;

    @PostMapping("/task")
    public TaskResponseDto saveTask(@RequestHeader("Authorization") String bearerToken, @RequestBody @Valid TaskDto taskDto) {
        return taskService.saveTask(bearerToken, taskDto);
    }

    @DeleteMapping("/task/{taskId}")
    public void deleteTask(@RequestHeader("Authorization") String bearerToken, @PathVariable Integer taskId) {
        taskService.deleteTask(bearerToken, taskId);
    }

    @GetMapping("/task")
    public List<TaskResponseDto> getAllTasks(@RequestHeader("Authorization") String bearerToken) {
        return taskService.getAllTasks(bearerToken);
    }

    @GetMapping("/task/{taskId}")
    public TaskResponseDto getTaskById(@RequestHeader("Authorization") String bearerToken, @PathVariable Integer taskId) {
        return taskService.getTaskById(bearerToken, taskId);
    }

    @PutMapping("/task/{taskId}")
    public TaskResponseDto updateTask(@RequestHeader("Authorization") String bearerToken, @PathVariable Integer taskId, @RequestBody @Valid TaskDto taskDto) {
        return taskService.updateTask(bearerToken, taskId, taskDto);
    }
}
