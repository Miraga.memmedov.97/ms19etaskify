package Ingress.az.eTaskify.controller;

import Ingress.az.eTaskify.dto.OrganizationDto;
import Ingress.az.eTaskify.dto.SignUpRequestDto;
import Ingress.az.eTaskify.dto.SignUpResponseDto;
import Ingress.az.eTaskify.service.OrganizationService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class OrganizationController {
    private final OrganizationService organizationService;

    @PostMapping("/organization")
    public SignUpResponseDto saveOrganizationAndUser(@RequestBody @Valid SignUpRequestDto signUpRequestDto) {
        return organizationService.saveOrganizationAndUser(signUpRequestDto);
    }

    @PutMapping("/organization")
    public OrganizationDto updateOrganization(@RequestHeader("Authorization") String bearerToken, @RequestBody @Valid OrganizationDto organizationDto) {
        return organizationService.updateOrganization(bearerToken, organizationDto);
    }



    @GetMapping("/organization")
    public OrganizationDto getOrganizationById(@RequestHeader("Authorization") String bearerToken) {
        return organizationService.getOrganizationById(bearerToken);
    }

    @DeleteMapping("/organization")
    public void deleteOrganization(@RequestHeader("Authorization") String bearerToken) {
        organizationService.deleteOrganization(bearerToken);
    }
}
