package Ingress.az.eTaskify.controller;
import Ingress.az.eTaskify.dto.UserRequestDto;
import Ingress.az.eTaskify.dto.UserResponseDto;
import Ingress.az.eTaskify.service.UserService;
import jakarta.validation.Valid;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1")
public class UserController {
    private final UserService userService;

    @PostMapping("/user")

    public UserResponseDto saveUser(@RequestHeader("Authorization") String bearerToken, @RequestBody @Valid UserRequestDto userRequestDto) {
        return userService.saveUser(bearerToken, userRequestDto);
    }


    @PutMapping("/user/{userId}")
    public UserResponseDto updateUser(@RequestHeader("Authorization") String bearerToken, @PathVariable Integer userId, @RequestBody @Valid UserRequestDto userRequestDto) {
        return userService.updateUser(bearerToken, userId, userRequestDto);
    }

    @GetMapping("/user")
    public List<UserResponseDto> getAllUsers(@RequestHeader("Authorization") String bearerToken) {
        return userService.getAllUsers(bearerToken);
    }

    @GetMapping("/{userId}")
    public UserResponseDto getUserById(@RequestHeader("Authorization") String bearerToken, @PathVariable Integer userId) {
        return userService.getUserById(bearerToken, userId);
    }

    @DeleteMapping("/user/{userId}")
    public void deleteUser(@RequestHeader("Authorization") String bearerToken, @PathVariable Integer userId) {
        userService.deleteUser(bearerToken, userId);
    }
}
