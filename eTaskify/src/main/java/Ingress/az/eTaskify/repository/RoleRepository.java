package Ingress.az.eTaskify.repository;

import Ingress.az.eTaskify.enums.UserRole;
import Ingress.az.eTaskify.model.Role;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface RoleRepository extends JpaRepository<Role, Integer> {
    Optional<Role> findByName(UserRole userRole);
}
