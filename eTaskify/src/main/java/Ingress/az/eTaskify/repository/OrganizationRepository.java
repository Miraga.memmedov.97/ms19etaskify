package Ingress.az.eTaskify.repository;

import Ingress.az.eTaskify.model.Organization;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface OrganizationRepository extends JpaRepository<Organization, Integer> {
    @Query(value = "select b from Organization b left join fetch b.userList left join b.taskList t where b.id=:id")
    Optional<Organization> findById(Integer id);

    @Query(value = "select b from Organization b left join fetch b.userList left join b.taskList t")
    List<Organization> findAll();

    @Query("select b from Organization b left join fetch b.userList left join b.taskList t where b.name=:name")
    Optional<Organization> findByName(String name);
}
