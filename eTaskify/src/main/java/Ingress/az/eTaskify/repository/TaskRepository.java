package Ingress.az.eTaskify.repository;

import Ingress.az.eTaskify.model.Task;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface TaskRepository extends JpaRepository<Task,Integer> {
    @Query(value = "select t from Task t join fetch t.organization o join fetch t.assignedUsers where t.organization.id=:organizationId")
    List<Task> findAllByOrganizationId(Integer organizationId);

    @Query(value = "select t from Task t join fetch t.organization o join fetch t.assignedUsers where t.organization.id=:organizationId and t.id=:taskId")
    Optional<Task> findByIdAndOrganizationId(Integer organizationId, Integer taskId);
}
