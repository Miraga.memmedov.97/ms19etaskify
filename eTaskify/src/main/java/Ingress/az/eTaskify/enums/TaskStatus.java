package Ingress.az.eTaskify.enums;

public enum TaskStatus {
    TODO,
    IN_PROGRESS,
    DONE,
    CANCELLED,
    ON_HOLD;
}
