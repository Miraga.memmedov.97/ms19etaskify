package Ingress.az.eTaskify.dto;

import Ingress.az.eTaskify.enums.TaskStatus;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.time.LocalDate;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class TaskResponseDto {
    Integer id;
    String title;
    String description;
    LocalDate deadTime;
    @Enumerated(EnumType.STRING)
    TaskStatus status;
    List<UserResponseDto> assignedUsers;
    OrganizationDto organization;
}
