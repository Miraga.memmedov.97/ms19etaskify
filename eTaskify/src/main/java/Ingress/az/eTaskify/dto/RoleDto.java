package Ingress.az.eTaskify.dto;

import Ingress.az.eTaskify.enums.UserRole;
import jakarta.persistence.Enumerated;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class RoleDto {
    @Enumerated
    @NotBlank
    UserRole name;
}
