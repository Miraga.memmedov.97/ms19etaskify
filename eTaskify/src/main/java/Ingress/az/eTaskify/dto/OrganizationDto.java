package Ingress.az.eTaskify.dto;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;


@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class  OrganizationDto {
    @NotBlank
    String name;
    @NotBlank
    String phone;
    @NotBlank
    String address;

}
