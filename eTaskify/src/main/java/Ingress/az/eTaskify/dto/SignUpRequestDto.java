package Ingress.az.eTaskify.dto;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class SignUpRequestDto {
    @NotBlank
    String name;
    @NotBlank
    String phone;
    @NotBlank
    String address;
    @NotBlank
    String username;
    String password;
    @Email
    String email;
}
