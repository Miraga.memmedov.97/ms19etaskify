package Ingress.az.eTaskify.dto;

import Ingress.az.eTaskify.model.Organization;
import Ingress.az.eTaskify.model.Role;
import Ingress.az.eTaskify.model.Task;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;
import lombok.experimental.FieldDefaults;

import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class UserDto {
    String name;
    String surname;
    String username;
    String password;
    String email;
    @JsonIgnore
    Organization organization;

    @JsonIgnore
    List<Role> roles;

    @JsonIgnore
    List<Task> tasks;
}
