package Ingress.az.eTaskify.dto;

import lombok.*;
import lombok.experimental.FieldDefaults;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@FieldDefaults(level = AccessLevel.PRIVATE)
public class OrganizationResponseDto {
    Integer id;
    String name;
    String phone;
    String address;

}
