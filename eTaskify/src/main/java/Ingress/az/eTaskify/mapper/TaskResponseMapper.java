package Ingress.az.eTaskify.mapper;

import Ingress.az.eTaskify.dto.TaskResponseDto;
import Ingress.az.eTaskify.model.Task;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface TaskResponseMapper {
    TaskResponseDto mapToTaskResponseDto(Task task);
    Task mapToTask(TaskResponseDto taskResponseDto);
    List<TaskResponseDto> mapToTaskResponseDtoList(List<Task> taskList);
}
