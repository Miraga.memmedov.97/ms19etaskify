package Ingress.az.eTaskify.mapper;

import Ingress.az.eTaskify.dto.OrganizationResponseDto;
import Ingress.az.eTaskify.model.Organization;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface OrganizationResponseMapper {
    OrganizationResponseDto mapToOrganizationResponseDto(Organization organization);
    Organization mapToOrganization(OrganizationResponseDto organizationResponseDto);
    List<OrganizationResponseDto> mapToOrganizationResponseDtoList(List<Organization> organizationList);
}
