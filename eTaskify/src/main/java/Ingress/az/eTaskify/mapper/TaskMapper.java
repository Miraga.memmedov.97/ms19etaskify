package Ingress.az.eTaskify.mapper;

import Ingress.az.eTaskify.dto.TaskDto;
import Ingress.az.eTaskify.model.Task;
import org.mapstruct.Mapper;

import java.util.List;
@Mapper(componentModel = "spring")
public interface TaskMapper {
    TaskDto mapToTaskDto(Task task);
    Task mapToTask(TaskDto taskDto);
    List<TaskDto> mapToTaskDtoList(List<Task> taskList);
}
