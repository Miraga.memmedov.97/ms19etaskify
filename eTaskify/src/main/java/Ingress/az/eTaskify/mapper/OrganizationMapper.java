package Ingress.az.eTaskify.mapper;

import Ingress.az.eTaskify.dto.OrganizationDto;
import Ingress.az.eTaskify.model.Organization;
import org.mapstruct.Mapper;

import java.util.List;
@Mapper(componentModel = "spring")
public interface OrganizationMapper {
    OrganizationDto mapToOrganizationDto(Organization organization);
    Organization mapToOrganization(OrganizationDto organizationDto);
    List<OrganizationDto> mapToOrganizationDtoList(List<Organization> organizationList);
}
