package Ingress.az.eTaskify.mapper;

import Ingress.az.eTaskify.dto.UserRequestDto;
import Ingress.az.eTaskify.model.User;
import org.mapstruct.Mapper;

import java.util.List;
@Mapper(componentModel = "spring")
public interface UserRequestMapper {
    UserRequestDto mapToUserRequestDto(User user);
    User mapToUser(UserRequestDto userRequestDto);
    List<UserRequestDto> mapToUserRequestDtoList(List<User> userList);
}
