package Ingress.az.eTaskify.mapper;

import Ingress.az.eTaskify.dto.UserResponseDto;
import Ingress.az.eTaskify.model.User;
import org.mapstruct.Mapper;

import java.util.List;
@Mapper(componentModel = "spring")
public interface UserResponseMapper {
    UserResponseDto mapToUserResponseDto(User user);
    User mapToUser(UserResponseDto userResponseDto);
    List<UserResponseDto> mapToUserResponseDtoList(List<User> userList);
}
