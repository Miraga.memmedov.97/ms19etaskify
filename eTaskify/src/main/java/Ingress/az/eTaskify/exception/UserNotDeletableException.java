package Ingress.az.eTaskify.exception;

public class UserNotDeletableException extends RuntimeException{
    public UserNotDeletableException(String msg) {
        super(msg);
    }
}
