package Ingress.az.eTaskify.exception;

public class CustomJwtException extends RuntimeException{
    public CustomJwtException(String msg) {
        super(msg);
    }
}
