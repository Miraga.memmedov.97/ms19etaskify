package Ingress.az.eTaskify.service;

import Ingress.az.eTaskify.dto.AuthRequestDto;
import Ingress.az.eTaskify.dto.JwtResponseDto;
import Ingress.az.eTaskify.dto.RefreshTokenRequestDto;

public interface AuthService {
    JwtResponseDto login(AuthRequestDto authRequestDto);

    JwtResponseDto refreshToken(RefreshTokenRequestDto refreshTokenRequestDto);
}
