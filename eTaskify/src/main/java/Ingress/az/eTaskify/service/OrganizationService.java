package Ingress.az.eTaskify.service;

import Ingress.az.eTaskify.dto.OrganizationDto;
import Ingress.az.eTaskify.dto.SignUpRequestDto;
import Ingress.az.eTaskify.dto.SignUpResponseDto;
import jakarta.transaction.Transactional;

public interface OrganizationService {
    @Transactional
    SignUpResponseDto saveOrganizationAndUser(SignUpRequestDto signUpRequestDto);

    OrganizationDto updateOrganization(String bearerToken, OrganizationDto organizationDto);

    OrganizationDto getOrganizationById(String bearerToken);

    void deleteOrganization(String bearerToken);
}
