package Ingress.az.eTaskify.service;

import Ingress.az.eTaskify.dto.AuthRequestDto;
import Ingress.az.eTaskify.dto.JwtResponseDto;
import Ingress.az.eTaskify.dto.RefreshTokenRequestDto;
import Ingress.az.eTaskify.exception.ResourceNotFoundException;
import Ingress.az.eTaskify.model.RefreshToken;
import Ingress.az.eTaskify.model.User;
import Ingress.az.eTaskify.security.JwtTokenProvider;
import lombok.AllArgsConstructor;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Service;

@Service
@AllArgsConstructor
public class AuthServiceImpl implements AuthService{
    private final AuthenticationManager authenticationManager;
    private final JwtTokenProvider jwtTokenProvider;
    private final UserService userService;
    private final RefreshTokenService refreshTokenService;

    @Override
    public JwtResponseDto login(AuthRequestDto authRequestDto) {

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(
                authRequestDto.getUsername(),
                authRequestDto.getPassword()
        ));

        User byUsername = userService.findByUsername(authRequestDto.getUsername());
        RefreshToken refreshToken = refreshTokenService.createRefreshToken(authRequestDto.getUsername());
        return JwtResponseDto.builder()
                .accessToken(jwtTokenProvider.generateToken(byUsername))
                .token(refreshToken.getToken()).build();

    }

    @Override
    public JwtResponseDto refreshToken(RefreshTokenRequestDto refreshTokenRequestDto) {
        return refreshTokenService.findByToken(refreshTokenRequestDto.getToken())
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser)
                .map(user -> {
                    String accessToken = jwtTokenProvider.generateToken(user);
                    return JwtResponseDto.builder()
                            .accessToken(accessToken)
                            .token(refreshTokenRequestDto.getToken()).build();
                }).orElseThrow(() ->new ResourceNotFoundException("Refresh Token is not in DB..!!"));

    }
}
