package Ingress.az.eTaskify.service;

import Ingress.az.eTaskify.exception.ResourceNotFoundException;
import Ingress.az.eTaskify.model.RefreshToken;
import Ingress.az.eTaskify.repository.RefreshTokenRepository;
import Ingress.az.eTaskify.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Optional;
import java.util.UUID;

@Service
@RequiredArgsConstructor
public class RefreshTokenServiceImpl implements RefreshTokenService{

    private final UserRepository userRepository;
    private final RefreshTokenRepository refreshTokenRepository;

    @Value("${security.jwt-refresh-expiration-milliseconds}")
    private long jwtRefreshExpirationDate;

    @Override
    public RefreshToken createRefreshToken(String username) {
        RefreshToken refreshToken = RefreshToken.builder()
                .user(userRepository.findByUsername(username).get())
                .token(UUID.randomUUID().toString())
                .expiryDate(LocalDateTime.now().plus(Duration.ofMillis(jwtRefreshExpirationDate)))
                .build();
        return refreshTokenRepository.save(refreshToken);
    }

    @Override
    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    @Override
    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().isBefore(LocalDateTime.now())) {
            refreshTokenRepository.delete(token);
            throw new ResourceNotFoundException(token.getToken() + " Refresh token is expired. Please make a new login..!");
        }
        return token;
    }
}
