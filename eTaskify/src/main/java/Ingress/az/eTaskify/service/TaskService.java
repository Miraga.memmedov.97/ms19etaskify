package Ingress.az.eTaskify.service;

import Ingress.az.eTaskify.dto.TaskDto;
import Ingress.az.eTaskify.dto.TaskResponseDto;

import java.util.List;

public interface TaskService {
    TaskResponseDto saveTask(String bearerToken, TaskDto taskDto);

    TaskResponseDto updateTask(String bearerToken, Integer taskId, TaskDto taskDto);

    List<TaskResponseDto> getAllTasks(String bearerToken);

    TaskResponseDto getTaskById(String bearerToken, Integer taskId);

    void deleteTask(String bearerToken, Integer taskId);
}
