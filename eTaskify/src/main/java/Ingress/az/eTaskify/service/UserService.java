package Ingress.az.eTaskify.service;

import Ingress.az.eTaskify.dto.UserRequestDto;
import Ingress.az.eTaskify.dto.UserResponseDto;
import Ingress.az.eTaskify.model.User;

import java.util.List;

public interface UserService {
    UserResponseDto saveUser(String bearerToken, UserRequestDto userRequestDto);

    UserResponseDto updateUser(String bearerToken, Integer userId, UserRequestDto userRequestDto);

    List<UserResponseDto> getAllUsers(String bearerToken);

    UserResponseDto getUserById(String bearerToken, Integer userId);

    void deleteUser(String bearerToken, Integer userId);

    User findByUsername(String username);
}
