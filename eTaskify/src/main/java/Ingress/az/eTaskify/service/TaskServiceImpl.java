package Ingress.az.eTaskify.service;

import Ingress.az.eTaskify.dto.TaskDto;
import Ingress.az.eTaskify.dto.TaskResponseDto;
import Ingress.az.eTaskify.email.EmailService;
import Ingress.az.eTaskify.exception.ResourceNotFoundException;
import Ingress.az.eTaskify.mapper.TaskMapper;
import Ingress.az.eTaskify.mapper.TaskResponseMapper;
import Ingress.az.eTaskify.model.Organization;
import Ingress.az.eTaskify.model.Task;
import Ingress.az.eTaskify.model.User;
import Ingress.az.eTaskify.repository.OrganizationRepository;
import Ingress.az.eTaskify.repository.TaskRepository;
import Ingress.az.eTaskify.repository.UserRepository;
import Ingress.az.eTaskify.security.JwtTokenProvider;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService{
    private final TaskRepository taskRepository;
    private final OrganizationRepository organizationRepository;
    private final UserRepository userRepository;
    private final TaskMapper taskMapper;
    private final TaskResponseMapper taskResponseMapper;
    private final EmailService emailService;
    private final JwtTokenProvider jwtTokenProvider;

    @Override
    public TaskResponseDto saveTask(String bearerToken, TaskDto taskDto) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        Task task = taskMapper.mapToTask(taskDto);
        Organization organizationDb = organizationRepository.findById(organizationId).orElseThrow(() -> new ResourceNotFoundException("Not found Organization with id = " + organizationId));

        List<User> assignedUsers = taskDto.getAssignedUsers().stream()
                .map(userDto -> userRepository.findByIdAndOrganizationId(userDto.getId(), organizationId)
                        .orElseThrow(() -> new ResourceNotFoundException("User not found: " + userDto.getId())))
                .collect(Collectors.toList());

        task.setAssignedUsers(assignedUsers);
        task.setOrganization(organizationDb);
        TaskResponseDto responseDto = taskResponseMapper.mapToTaskResponseDto(taskRepository.save(task));

        for (User assignedUser:assignedUsers)
        {
            emailService.sendEmail(assignedUser.getEmail(),task.getTitle(),"Dear "+assignedUser.getName()+"\n" +
                    "\n" +
                    "I hope this email finds you well.\n" +
                    "\n" +
                    "I wanted to inform you that a new task has been assigned to you.");

        }
        return responseDto;

    }

    @Override
    public TaskResponseDto updateTask(String bearerToken, Integer taskId, TaskDto taskDto) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        Task task = taskMapper.mapToTask(taskDto);
        Task taskDb = taskRepository.findByIdAndOrganizationId(organizationId, taskId).orElseThrow(() -> new ResourceNotFoundException("Not found Task with id = " + taskId));

        task.setId(taskId);
        task.setOrganization(taskDb.getOrganization());

        List<User> assignedUsers = taskDto.getAssignedUsers() != null ?
                taskDto.getAssignedUsers().stream()
                        .map(userDto -> userRepository.findByIdAndOrganizationId(userDto.getId(), organizationId)
                                .orElseThrow(() -> new ResourceNotFoundException("User not found: " + userDto.getId())))
                        .collect(Collectors.toList()) :
                taskRepository.findById(taskId)
                        .orElseThrow(() -> new ResourceNotFoundException("Not found Task with id = " + taskId))
                        .getAssignedUsers();

        task.setAssignedUsers(assignedUsers);

        return taskResponseMapper.mapToTaskResponseDto(taskRepository.save(task));
    }

    @Override
    public List<TaskResponseDto> getAllTasks(String bearerToken) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        return taskResponseMapper.mapToTaskResponseDtoList(taskRepository.findAllByOrganizationId(organizationId));
    }

    @Override
    public TaskResponseDto getTaskById(String bearerToken, Integer taskId) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        Task taskDb = taskRepository.findByIdAndOrganizationId(organizationId, taskId).orElseThrow(() -> new ResourceNotFoundException("Not found Task with id = " + taskId));
        return taskResponseMapper.mapToTaskResponseDto(taskDb);
    }

    @Override
    public void deleteTask(String bearerToken, Integer taskId) {
        Integer organizationId = jwtTokenProvider.getOrganizationId(bearerToken.substring(7, bearerToken.length()));
        taskRepository.findByIdAndOrganizationId(organizationId, taskId).orElseThrow(() -> new ResourceNotFoundException("Not found Task with id = " + taskId));
        taskRepository.deleteById(taskId);
    }
}
